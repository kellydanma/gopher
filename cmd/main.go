package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"

	"gitlab.com/kellydanma/gopher/story"
)

var (
	flagJSONPath string
	flagPort     int
)

func init() {
	flag.StringVar(&flagJSONPath, "file", "gopher.json", "path/to/JSON_file that contains the CYOA story")
	flag.IntVar(&flagPort, "port", 3000, "the port to start the web application on")
	flag.Parse()
}

func main() {
	f, err := os.Open(flagJSONPath)
	if err != nil {
		exit(fmt.Sprintf("cannot open JSON file: %s", err.Error()))
	}

	s, err := story.JSONStory(f)
	if err != nil {
		exit(fmt.Sprintf("cannot decode story: %s", err.Error()))
	}

	h := story.NewHandler(s)
	fmt.Printf("Starting the server on port: %d\n", flagPort)
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", flagPort), h))
}

func exit(msg string) {
	fmt.Println(msg)
	os.Exit(1)
}
