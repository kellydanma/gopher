# gopher

This is a choose-your-own-adventure project to learn about web applications.

## Usage

The web application runs on `http://localhost:3000` by default:

```
$ go run cmd/main.go
Starting the server on port: 3000
```

You can then navigate to a browser to choose your own adventure.

![intro page](./assets/gopher.PNG "Intro page for CYOA")

## Customize

| Flag          | Default         | Usage  |       Example      |
|:-------------:| --------------- | ------ | ------------------ |
| `file`        | `gopher.json`   | Indicates the JSON file containing the CYOA | `go run cmd/main.go -file="yourFile.json"` selects a different file |
| `port`        | `3000`          | Specifies the port to start the web application on | `go run cmd/main.go -port=8080` starts the web application on `localhost:8080` |
